# Ejercicios Jekyll

## 1. Analizaremos el proyecto Jekyll básico en el repositorio: Proyecto Básico en Jekyll.
### Añade dos página adicionales al proyecto, y haz que se muestren junto con la página About.
Se ha añadido las páginas adicionales `Page 1` y `Page 2` localizadas en `page-1.md` y `page-2.md`, respectivamente.

| Páginas adicionales |
|------|
| ![Páginas adicionales](./docs/img/addional-pages.png) |

### Añade otro post al proyecto, y haz que se muestre en la página index.html
Se ha añadido el fichero `_posts/2019-06-26-new-post.md`, con la siguiente implementación:
```
---
layout: post
title:  "New post!"
date:   2019-06-26 12:58:29
categories: jekyll update
---
New post!
```

Inicialmente, he intentado poner la fecha `2020-06-26`, pero por alguna razón que desconozco no generaba el correspondiente código html en `_site`.

| Listado de posts | Página del nuevo post |
|------|------|
| ![Listado de posts](./docs/img/new-post.png) | ![Página del nuevo post](./docs/img/new-post-page.png) |

## 2. Identifica objetos, filtros y etiquetas en el siguiente fragmento de código e indica cuál es u finalidad.

```html
{% assign episodes = site.episodes | sort: 'weight' %}  
{% for episode in episodes limit: 6 %}  
    <div class="grid__cell grid__cell--33">
        {% include episode_preview.html episode=episode %}
    </div>
{% endfor %}
```
Se asigna el objeto con los episodios del sitio web, haciendo uso de la etiqueta `assign`, ordenados por su peso mediante el filtro `sort`. Posteriormente se itera sobre los primeros 6 episodios, haciendo uso de la etiqueta iterativa `for` y `limit`, los cuales se pasarán como parámetro a la plantilla `episode_preview.html`.


## 3. Qué código html tendría como salida:
```html
<html>   
    <head>
        <title>{{ page.title }} -Ejemplo Jekyll </title>
    </head>
    <body> 
        {% include nav.html %}` 
        {{ content }}
    </body>  
</html>
```

En el caso del header, se tendría en el tag `title` el título de la página concatenado a `-Ejemplo Jekyll`. Y en caso del contenido de la página, en el tag `body`, se tendrá el código html asociado a `nav.html` y código html que se genere para esa página.

## 4. Especifica los filtros, variales Jekyll, tags, y qué se consigue con el siguiente código.
```html
<footer id="footer">
    <p class="small">© Copyright {{ site.time | date: '%Y' }} {{ site.author }}</p>
</footer>
```
Se usan el filtro `date` para obtener el año de la fecha del sitio web, y aparte de la fecha, se utiliza la varibale `author` del sitio. El propósito del código es especificar los derechos de autor en el footer de la página.


## 5 A. Indicar qué scripts se están invocando con el siguiente código:
```html
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="{{ "/" | relative_url  }}assets/js/main.js"></script>
<script src="{{ "/" | relative_url  }}assets/js/highlight.js"></script>
```
Se está incoporando `jQuery` a partir de una url externa, y los scripts `main.js` y `highlight.js` del sitio web. La incoporación de los scripts del propio sitio funciona independientemente del `baseUrl` del sitio web, puesto que el filtro `relative_url` lo añade de forma transparente.

## 5 B. Indica el resultado que se obtiene con el siguiente código, sabiendo que `paginas` corresponde la colección almacenada en `_characters`,
```html
<ul class="nav">
    {% assign paginas = site.paginas | sort: 'name' %}
    {% for pagina in paginas %}
        <li class="nav__item">
            <a href="{{ pagina.url }}">{{ pagina.title }}</a>
        </li>
    {% endfor %}
</ul>
```
Se obtiene como resultado un menu de navegación de las personajes ordenadas por su nombre, de forma ascendente distinguiendo mayúsculas y minúsculas.

## 6. Dada una colección de documentos con las siguientes variables en el frontmatter:
```
title: Mi Pagina
thumbnail_url:  https://XXX
image_url: https://
```

Indicar qué objetivo tiene el siguiente include:

```html
<a href="{{ character.url }}" class="character-preview">
  <img src="{{ character.thumbnail_url }}" class="character-preview__image" />
  <div class="character-preview__label">
    {{ character.title }}
  </div>
</a>
```
Mostrar un enlace, formado por la imagen en miniatura y el título del personaje, que apunta a la página del personaje. Nótese que no hace falta declarar el campo `url` del personaje en el `frontmatter`, pues este se genera automáticamente a partir de una configuración por defecto o según se especifique en la colección mediante el campo `permalink`.
